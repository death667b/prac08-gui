/**
 * 
 */
package guiExploration;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import java.awt.*;

/**
 * @author user
 *
 */
public class PanelsAndWidgets extends JFrame implements ActionListener, Runnable {

	private static final long serialVersionUID = -7031008862559936404L;
	public static final int WIDTH = 300;
	public static final int HEIGHT = 200;
	
	private JPanel pnlOne;
	private JPanel pnlTwo;
	private JPanel pnlThree;
	private JPanel pnlBtn;
	private JPanel pnlFive;
	/**
	 * 
	 */
	public PanelsAndWidgets(String name) {
		// TODO Auto-generated constructor stub
		super(name);
		createGUI();
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		SwingUtilities.invokeLater(new PanelsAndWidgets("BorderLayout"));

	}
	
	private void createGUI() { setSize(WIDTH, HEIGHT); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setLayout(new BorderLayout());
		//Panel related code will go here
		
		uglyLayoutCode();		
		layoutButtonPanel();
		
		repaint();
		this.setVisible(true);
	}
	
	private void layoutButtonPanel() {
		GridBagLayout layout = new GridBagLayout();
		pnlBtn.setLayout(layout);
		//Lots of layout code here
		
		pnlBtn.add(createButton("test"));
		//btnLoad = createButton(�Load�);
		
	}

	/**
	 * 
	 */
	private void uglyLayoutCode() {
		pnlOne = createPanel(Color.WHITE);
		pnlTwo = createPanel(Color.LIGHT_GRAY);
		pnlThree = createPanel(Color.LIGHT_GRAY);
		pnlBtn = createPanel(Color.LIGHT_GRAY);
		pnlFive = createPanel(Color.LIGHT_GRAY);	
		
		this.getContentPane().add(pnlOne,BorderLayout.CENTER);
		this.getContentPane().add(pnlTwo,BorderLayout.NORTH);
		this.getContentPane().add(pnlThree,BorderLayout.EAST);
		this.getContentPane().add(pnlBtn,BorderLayout.SOUTH);
		this.getContentPane().add(pnlFive,BorderLayout.WEST);
	}

	private JPanel createPanel(Color c) {
		//Create a JPanel object and store it in a local var
		//set the background colour to that passed in c
		//Return the JPanel object
	    JPanel returnPanel = new JPanel();
	    returnPanel.setBackground(c);
		
		return returnPanel;
	}
	
	private JButton createButton(String str) {
		//Create a JButton object and store it in a local var
		//Set the button text to that passed in str
		//Add the frame as an actionListener
		//Return the JButton object
		JButton returnButton = new JButton(str);
		returnButton.addActionListener(null);
		return returnButton;
	}
}
